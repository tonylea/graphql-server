# GraphQL server

## Database structure

Table1: user

- id: INT!
- AccountExpirationDate: DATETIME
- AllowReversiblePasswordEncryption: BOO 
- badPwdCount: INT
- CannotChangePassword: BOOL
- City: STRING
- Company: STRING
- Created: DATETIME
- Deleted: DATETIME
- Department: STRING
- DisplayName: STRING
- DistinguishedName: STRING
- Division: STRING
- Enabled: BOOL
- GivenName: STRING
- Initials: STRING
- LastBadPasswordAttempt: DATETIME
- lastLogoff: DATETIME
- LastLogonDate: DATETIME
- LockedOut: BOOL
- lockoutTime: DATETIME
- logonCount: INT
- mail: STRING
- mailNickname: STRING
- Manager: STRING
- MobilePhone: STRING
- Modified: DATETIME
- Name: STRING
- ObjectGUID: STRING
- Office: STRING
- OfficePhone: STRING
- OtherName: STRING
- PasswordExpired: 
- PasswordLastSet: DATETIME
- PasswordNeverExpires: BOOL
- PasswordNotRequired: BOOL
- PostalCode: STRING
- PrimaryGroupId: STRING
- SamAccountName: STRING
- State: STRING
- StreetAddress: STRING
- Surname: STRING
- telephoneNumber: STRING
- Title: STRING

Table2: Groups
- id: INT
- adminCount: INT
- createTimeStamp: DATETIME
- Deleted: BOOL
- Description: STRING
- DisplayName: STRING
- DistinguishedName: STRING
- ManagedBy: INT (User Id)
- modifyTimeStamp: DATETIME
- Name: STRING
- ObjectGUID: STRING
- SamAccountName: STRING

Table3: UserGroupMembership

- UserId: INT (User ID)
- GroupId: INT (Group ID)

Table4: GroupGroupMembership

- GroupMemberId: INT (Group ID)
- GroupGroupId: INT (Group ID)

