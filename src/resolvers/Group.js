export default {
  Query: {
    getGroup: (parent, { id }, { models }) => models.Group.findOne({ where: { id } }),
    allGroups: (parent, args, { models }) => models.Group.findAll()
  },
  Mutation: {
    createGroup: (parent, args, { models }) => models.Group.create(args)
  }
};
