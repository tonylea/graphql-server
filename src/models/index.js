import Sequelize from 'sequelize';

const config = {
  database: 'copperpond',
  username: 'svc-copperpond',
  password: 'SecretPassword'
};

const opts = {
  dialect: 'mssql',
  host: 'localhost',
  port: 1433,
  define: {
    freezeTableName: true,
    underscoredAll: true,
    underscored: true,
    timestamps: false
  }
};

const sequelize = new Sequelize(config.database, config.username, config.password, opts);

const models = {
  User: sequelize.import('./user'),
  Group: sequelize.import('./group')
};

Object.keys(models).forEach(modelName => {
  if ('associate' in models[modelName]) {
    models[modelName].associate(models);
  }
});

models.sequelize = sequelize;
models.Sequelize = Sequelize;

export default models;
