export default (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    city: { type: DataTypes.STRING },
    company: { type: DataTypes.STRING },
    department: { type: DataTypes.STRING },
    displayName: { type: DataTypes.STRING, field: 'display_name' },
    distinguishedName: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      field: 'distinguished_name'
    },
    division: { type: DataTypes.STRING },
    givenName: { type: DataTypes.STRING, field: 'given_name' },
    initials: { type: DataTypes.STRING },
    mail: { type: DataTypes.STRING },
    mailNickname: { type: DataTypes.STRING, field: 'mail_nickname' },
    mobilePhone: { type: DataTypes.STRING, field: 'mobile_phone' },
    name: { type: DataTypes.STRING },
    objectGUID: {
      type: DataTypes.STRING,
      unique: true,
      primaryKey: true,
      allowNull: false,
      field: 'object_GUID'
    },
    office: { type: DataTypes.STRING },
    officePhone: { type: DataTypes.STRING, field: 'office_phone' },
    otherName: { type: DataTypes.STRING, field: 'other_name' },
    postalCode: { type: DataTypes.STRING, field: 'postal_code' },
    samAccountName: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      field: 'sam_account_name'
    },
    county: { type: DataTypes.STRING },
    streetAddress: { type: DataTypes.STRING, field: 'street_address' },
    surname: { type: DataTypes.STRING },
    telephoneNumber: { type: DataTypes.STRING, field: 'telephone_number' },
    jobTitle: { type: DataTypes.STRING, field: 'job_title' },

    whenCreated: { type: DataTypes.INTEGER, allowNull: false, field: 'when_created' },
    accountDeleted: { type: DataTypes.BOOLEAN, field: 'account_deleted' },
    accountEnabled: { type: DataTypes.BOOLEAN, field: 'account_enabled' },
    accountExpiration: { type: DataTypes.INTEGER, field: 'account_expiration' },
    whenChanged: { type: DataTypes.INTEGER, field: 'when_changed' },
    allowReversiblePasswordEncryption: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      field: 'allow_reversible_password_encryption'
    },
    badPasswordCount: { type: DataTypes.INTEGER, field: 'bad_password_count' },
    cannotChangePassword: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      field: 'cannot_change_password'
    },
    lastBadPasswordAttempt: { type: DataTypes.INTEGER, field: 'last_bad_password_attempt' },
    lastLogoff: { type: DataTypes.INTEGER, field: 'last_logoff' },
    lastLogonDate: { type: DataTypes.INTEGER, field: 'last_logon_date' },
    lockedOut: { type: DataTypes.BOOLEAN, field: 'locked_out' },
    lockoutTime: { type: DataTypes.INTEGER, field: 'lockout_time' },
    logonCount: { type: DataTypes.INTEGER, field: 'logon_count' },
    passwordExpired: { type: DataTypes.BOOLEAN, field: 'password_expired' },
    passwordLastSet: { type: DataTypes.INTEGER, field: 'password_last_set' },
    passwordNeverExpires: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      field: 'password_never_expires'
    },
    passwordNotRequired: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      field: 'password_not_required'
    }
  });

  User.associate = models => {
    User.belongsToMany(models.Group, {
      through: 'userGroupMembership',
      foreignKey: {
        name: 'userId',
        field: 'user_id'
      }
    });
  };

  return User;
};
