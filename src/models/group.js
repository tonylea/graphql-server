export default (sequelize, DataTypes) => {
  const Group = sequelize.define('Group', {
    deleted: { type: DataTypes.BOOLEAN },
    description: { type: DataTypes.STRING },
    displayName: { type: DataTypes.STRING, field: 'display_name' },
    distinguishedName: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      field: 'distinguished_name'
    },
    name: { type: DataTypes.STRING, unique: true, allowNull: false },
    objectGUID: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      field: 'object_GUID',
      primaryKey: true
    },
    samAccountName: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      field: 'sam_account_name'
    },
    whenChanged: { type: DataTypes.INTEGER, field: 'when_changed' },
    whenCreated: { type: DataTypes.INTEGER, field: 'when_created' }
  });

  Group.associate = models => {
    Group.belongsTo(models.User, {
      foreignKey: {
        name: 'managedBy',
        field: 'managed_by'
      }
    });
    Group.belongsToMany(models.User, {
      through: 'userGroupMembership',
      foreignKey: {
        name: 'groupId',
        field: 'group_id'
      }
    });
  };

  return Group;
};
