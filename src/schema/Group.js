export default `

  type Mutation {
    createGroup(
      deleted: Boolean
      description: String
      displayName: String
      distinguishedName: String!
      name: String!
      objectGUID: String!
      samAccountName: String!
      whenChanged: Int
      whenCreated: Int!
    ): Group!
  }

  type Query {
    getGroup(
      samAccountName: String!
    ): Group!
    allGroups: [Group!]!
  }

  type Group {
    deleted: Boolean
    description: String
    displayName: String
    distinguishedName: String!
    name: String!
    objectGUID: String!
    samAccountName: String!
    members: GroupMembers
    managedBy: User
    whenChanged: Int
    whenCreated: Int!
  }
`;
