export default `
  type User {
    city: String
    company: String
    department: String
    displayName: String!
    distinguishedName: String!
    division: String
    givenName: String
    initials: String
    mail: String
    mailNickname: String
    manager: User
    memberOf: [Group]
    mobilePhone: String
    name: String
    objectGUID: String!
    office: String
    officePhone: String
    otherName: String
    postalCode: String
    primaryGroup: Group!
    samAccountName: String!
    county: String
    streetAddress: String
    surname: String
    telephoneNumber: String
    jobTitle: String
    accountCreated: Int!
    accountDeleted: Int
    accountEnabled: Boolean
    accountExpirationDate: Int
    accountModified: Int
    allowReversiblePasswordEncryption: Boolean!
    badPasswordCount: Int
    cannotChangePassword: Boolean!
    lastBadPasswordAttempt: Int
    lastLogoff: Int
    lastLogonDate: Int
    lockedOut: Boolean
    lockoutTime: Int
    logonCount: Int
    passwordExpired: Boolean
    passwordLastSet: Int
    passwordNeverExpires: Boolean!
    passwordNotRequired: Boolean!
  }
`;
