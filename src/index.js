import express from 'express';
import bodyParser from 'body-parser';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { makeExecutableSchema } from 'graphql-tools';
import path from 'path';
import { fileLoader, mergeTypes, mergeResolvers } from 'merge-graphql-schemas';

import models from './models';

const PORT = 8080;
const GRAPHQL_ENDPOINT = '/graphql';
const GRAPHIQL_ENDPOINT = '/graphiql';

const typeDefs = mergeTypes(fileLoader(path.join(__dirname, './schema')));
const resolvers = mergeResolvers(fileLoader(path.join(__dirname, './resolvers')));

const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

const app = express();
app.get('/', (req, res) => res.status(200).send('Hello World!'));
app.use(
  GRAPHQL_ENDPOINT,
  bodyParser.json(),
  graphqlExpress({
    schema,
    context: {
      models
    }
  })
);
app.use(GRAPHIQL_ENDPOINT, graphiqlExpress({ endpointURL: GRAPHQL_ENDPOINT }));

models.sequelize.sync().then(() => {
  app.listen(PORT, () => console.log(`App listening on port ${PORT}`));
});
