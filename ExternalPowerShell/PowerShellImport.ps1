$ServerInstance = "localhost"
$Database = "copperpond"
$secpasswd = ConvertTo-SecureString "SecretPassword" -AsPlainText -Force
$Credential = New-Object System.Management.Automation.PSCredential ("svc-copperpond", $secpasswd)


# AD Groups
$AllGroups = Get-ADGroup -Properties * -Filter {Name -like "*"}
foreach ($Group in $AllGroups) {
  $Upload = $null
  $Upload = @{
    deleted           = if ($Group.Deleted) {1}else {0}
    description       = if ($Group.Description) {"'$($Group.Description -replace "'","''")'"} else {"NULL"}
    displayName       = if ($Group.DisplayName) {"'$($Group.DisplayName -replace "'","''")'"} else {"NULL"}
    distinguishedName = if ($Group.DistinguishedName) {"'$($Group.DistinguishedName -replace "'","''")'"} else {"NULL"}
    name              = if ($Group.Name) {"'$($Group.Name -replace "'","''")'"} else {"NULL"}
    objectGUID        = if ($Group.ObjectGUID) {"'$($Group.ObjectGUID -replace "'","''")'"} else {"NULL"}
    samAccountName    = if ($Group.SamAccountName) {"'$($Group.SamAccountName -replace "'","''")'"} else {"NULL"}
    whenChanged       = Get-Date -Date $Group.whenChanged -UFormat %s
    whenCreated       = Get-Date -Date $Group.whenCreated -UFormat %s
  }
  $Query = "INSERT INTO [Group] (deleted, description, display_name, distinguished_name, name, object_GUID, sam_account_name, when_changed, when_created) VALUES ($($Upload.deleted), $($Upload.description), $($Upload.displayName), $($Upload.distinguishedName), $($Upload.name), $($Upload.objectGUID), $($Upload.samAccountName), $($Upload.whenChanged), $($Upload.whenCreated))"
  Invoke-SqlCmd2 -ServerInstance $ServerInstance -Database $Database -Query $Query -Credential $Credential
}

# Users
$AllUsers = Get-ADUser -Properties * -Filter {Name -like "*"}

# User Address Properties
foreach ($User in $AllUsers) {
  $Upload = $null
  $Upload = @{
    city = if ($User.City) {"'$($User.City -replace  "'","''")'"} else {"NULL"}
    company = if ($User.company) {"'$($User.company -replace  "'","''")'"} else {"NULL"}
    department = if ($User.department) {"'$($User.department -replace  "'","''")'"} else {"NULL"}
    displayName = if ($User.displayName) {"'$($User.displayName -replace  "'","''")'"} else {"NULL"}
    distinguishedName = if ($User.distinguishedName) {"'$($User.distinguishedName -replace  "'","''")'"} else {"NULL"}
    division = if ($User.division) {"'$($User.division -replace  "'","''")'"} else {"NULL"}
    givenName = if ($User.givenName) {"'$($User.givenName -replace  "'","''")'"} else {"NULL"}
    initials = if ($User.initials) {"'$($User.initials -replace  "'","''")'"} else {"NULL"}
    mail = if ($User.mail) {"'$($User.mail -replace  "'","''")'"} else {"NULL"}
    mailNickname = if ($User.mailNickname) {"'$($User.mailNickname -replace  "'","''")'"} else {"NULL"}
    mobilePhone = if ($User.mobilePhone) {"'$($User.mobilePhone -replace  "'","''")'"} else {"NULL"}
    name = if ($User.name) {"'$($User.name -replace  "'","''")'"} else {"NULL"}
    objectGUID = if ($User.objectGUID) {"'$($User.objectGUID -replace  "'","''")'"} else {"NULL"}
    office = if ($User.office) {"'$($User.office -replace  "'","''")'"} else {"NULL"}
    officePhone = if ($User.officePhone) {"'$($User.officePhone -replace  "'","''")'"} else {"NULL"}
    otherName = if ($User.otherName) {"'$($User.otherName -replace  "'","''")'"} else {"NULL"}
    postalCode = if ($User.postalCode) {"'$($User.postalCode -replace  "'","''")'"} else {"NULL"}
    samAccountName = if ($User.samAccountName) {"'$($User.samAccountName -replace  "'","''")'"} else {"NULL"}
    county = if ($User.State) {"'$($User.State -replace  "'","''")'"} else {"NULL"}
    streetAddress = if ($User.streetAddress) {"'$($User.streetAddress -replace  "'","''" -replace "`r`n",", ")'"} else {"NULL"}
    surname = if ($User.surname) {"'$($User.surname -replace  "'","''")'"} else {"NULL"}
    telephoneNumber = if ($User.telephoneNumber) {"'$($User.telephoneNumber -replace  "'","''")'"} else {"NULL"}
    jobTitle = if ($User.Title) {"'$($User.Title -replace  "'","''")'"} else {"NULL"}
    accountDeleted = if ($User.Deleted) {1} else {0}
    accountEnabled = if ($User.Enabled) {1} else {0}
    allowReversiblePasswordEncryption = if ($User.allowReversiblePasswordEncryption) {1} else {0}
    cannotChangePassword = if ($User.cannotChangePassword) {1} else {0}
    lockedOut = if ($User.lockedOut) {1} else {0}
    passwordExpired = if ($User.passwordExpired) {1} else {0}
    passwordNeverExpires = if ($User.passwordNeverExpires) {1} else {0}
    passwordNotRequired = if ($User.passwordNotRequired) {1} else {0}
    whenCreated = Get-Date -Date $User.whenCreated -UFormat %s
    accountExpiration = if($User.accountExpirationDate) {Get-Date -Date $User.accountExpirationDate -UFormat %s} else {"NULL"}
    whenChanged = Get-Date -Date $User.whenChanged -UFormat %s
    badPasswordCount = if($User.badPasswordCount) {$User.badPasswordCount} else {"NULL"}
    lastBadPasswordAttempt = if($User.lastBadPasswordAttempt) {Get-Date -Date $User.lastBadPasswordAttempt -UFormat %s} else {"NULL"}
    lastLogoff = if($User.lastLogoff) {Get-Date -Date $User.lastLogoff -UFormat %s} else {"NULL"}
    lastLogonDate = if($User.lastLogonDate) {Get-Date -Date $User.lastLogonDate -UFormat %s} else {"NULL"}
    lockoutTime = if($User.lockoutTime) {Get-Date -Date ([datetime]::FromFileTime($User.lockoutTime)) -UFormat %s} else {"NULL"}
    logonCount = if($User.logonCount) {$User.logonCount} else {"NULL"}
    passwordLastSet= if($User.passwordLastSet) {Get-Date -Date $User.passwordLastSet -UFormat %s} else {"NULL"}   
  }

  $Query = "INSERT INTO [User] (city, company, department, display_name, distinguished_name, division, given_name, initials, mail, mail_nickname, mobile_phone, name, object_GUID, office, office_phone, other_name, postal_code, sam_account_name, county, street_address, surname, telephone_number, job_title, account_deleted, account_enabled, allow_reversible_password_encryption, cannot_change_password, locked_out, password_expired, password_never_expires, password_not_required, when_created, account_expiration, when_changed, bad_password_count, last_bad_password_attempt, last_logoff, last_logon_date, lockout_time, logon_count, password_last_set) VALUES ($($Upload.city), $($Upload.company), $($Upload.department), $($Upload.displayName), $($Upload.distinguishedName), $($Upload.division), $($Upload.givenName), $($Upload.initials), $($Upload.mail), $($Upload.mailNickname), $($Upload.mobilePhone), $($Upload.name), $($Upload.objectGUID), $($Upload.office), $($Upload.officePhone), $($Upload.otherName), $($Upload.postalCode), $($Upload.samAccountName), $($Upload.county), $($Upload.streetAddress), $($Upload.surname), $($Upload.telephoneNumber), $($Upload.jobTitle), $($Upload.accountDeleted), $($Upload.accountEnabled), $($Upload.allowReversiblePasswordEncryption), $($Upload.cannotChangePassword), $($Upload.lockedOut), $($Upload.passwordExpired), $($Upload.passwordNeverExpires), $($Upload.passwordNotRequired), $($Upload.whenCreated), $($Upload.accountExpiration), $($Upload.whenChanged), $($Upload.badPasswordCount), $($Upload.lastBadPasswordAttempt), $($Upload.lastLogoff), $($Upload.lastLogonDate), $($Upload.lockoutTime), $($Upload.logonCount), $($Upload.passwordLastSet))"

  Invoke-SqlCmd2 -ServerInstance $ServerInstance -Database $Database -Query $Query -Credential $Credential
}


# Group Membership

$GroupGuid = (Get-ADGroup -Identity UG_Infrastructure).ObjectGuid

$Members = Get-ADGroupMember -Identity $GroupGuid
foreach ($Member in $Members) {
  if ($Member.objectClass -like "user") {
    $Query = "INSERT INTO [userGroupMembership] (user_id, group_id) VALUES ('$($Member.objectGUID)', '$($GroupGuid)'"
    Invoke-SqlCmd2 -ServerInstance $ServerInstance -Database $Database -Query $Query -Credential $Credential
  }
}
